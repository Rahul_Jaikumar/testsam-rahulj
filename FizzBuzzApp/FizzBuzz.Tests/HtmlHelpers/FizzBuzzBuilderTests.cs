﻿namespace FizzBuzz.Tests.HtmlHelpers
{
    using Moq;
    using NUnit.Framework;
    using System.Web;
    using System.Web.Mvc;
    using FluentAssertions;
    using FizzBuzz.HtmlHelpers;

    internal class FizzBuzzBuilderTests
    {
        private HtmlHelper htmlHelper;
        [SetUp]
        public void Setup()
        {
            ViewDataDictionary viewdata = new ViewDataDictionary();
            Mock<ViewContext> mockViewContext = new Mock<ViewContext>();
            mockViewContext.Setup(o => o.ViewData).Returns(viewdata);
            Mock<IViewDataContainer> mockViewDataContainer = new Mock<IViewDataContainer>();
            mockViewDataContainer.Setup(v => v.ViewData).Returns(viewdata);
            htmlHelper = new HtmlHelper(mockViewContext.Object, mockViewDataContainer.Object);
        }

        [TestCase("Fizz", "<span class=\"Fizz\">Fizz</span>")]
        [TestCase("Buzz", "<span class=\"Buzz\">Buzz</span>")]
        [TestCase("Wizz", "<span class=\"Wizz\">Wizz</span>")]
        [TestCase("Wuzz", "<span class=\"Wuzz\">Wuzz</span>")]
        [TestCase("Fizz Buzz", "<span class=\"Fizz\">Fizz</span> <span class=\"Buzz\">Buzz</span>")]
        [TestCase("Wizz Wuzz", "<span class=\"Wizz\">Wizz</span> <span class=\"Wuzz\">Wuzz</span>")]
        public void FizzBuzzValueBuilderReturnsSpanTextWithSuitableClass(string fizzBuzzText, string expectedOutput)
        {
            //Act
            IHtmlString result = htmlHelper.FizzBuzzValueBuilder(fizzBuzzText);

            //Assert
            result.ToHtmlString().Should().Be(expectedOutput);
        }
    }
}