﻿namespace FizzBuzz.Tests.controllers
{
    using FizzBuzz.Business.Interfaces;
    using FizzBuzz.Controllers;
    using FizzBuzz.Models;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Web.Mvc;

    public class FizzBuzzControllerTests
    {
        private Mock<IFizzBuzzBusiness> businessLogicMock;
        private FizzBuzzController fizbuzzController;

        [SetUp]
        public void Setup()
        {
            businessLogicMock = new Mock<IFizzBuzzBusiness>();
            fizbuzzController = new FizzBuzzController(businessLogicMock.Object);
            businessLogicMock.Setup(m => m.GetFizzBuzzList(It.IsAny<int>())).Returns(new List<string>
            {  "1",
                "2",
                "Fizz",
                "4",
                "Buzz",
                "Fizz",
                "7",
                "8",
                "Fizz",
                "Buzz",
                "11",
                "Fizz",
                "13",
                "14",
                "Fizz Buzz" });
        }

        [Test]
        public void FizzbuzzreturnsFizzbuzzView()
        {
            // Act
            ViewResult viewPage = fizbuzzController.Fizzbuzz() as ViewResult;

            // Assert
            viewPage.ViewName.Should().Be("Fizzbuzz");
        }

        [Test]
        public void FizzBuzzPostMethodReturnsErrorForInvalidCase()
        {
            // Arrange
            fizbuzzController.ModelState.AddModelError("InputNumber", "Enter the number between 1 and 1000");
            FizzBuzz model = new FizzBuzz() { InputNumber = 1001 };

            // Act
            ViewResult viewResult = fizbuzzController.Fizzbuzz(model) as ViewResult;
            FizzBuzz actualModel = viewResult.Model as FizzBuzz;

            // Assert
            viewResult.Model.Should().Be(model);
            actualModel.DisplayList.Should().BeNull();
            viewResult.ViewName.Should().Be("Fizzbuzz");
        }

        [Test]
        public void FizzbuzzReturnsAListOfValuesWhenNumberIsSubmitted()
        {
            // Arrange
            FizzBuzz fizbuzzModel = new FizzBuzz { InputNumber = 15 };
            IList<string> expectedOutput = new List<string>
            {
                "1",
                "2",
                "Fizz",
                "4",
                "Buzz",
                "Fizz",
                "7",
                "8",
                "Fizz",
                "Buzz",
                "11",
                "Fizz",
                "13",
                "14",
                "Fizz Buzz" };

            // Act
            ActionResult result = fizbuzzController.Fizzbuzz(fizbuzzModel);
            ViewResult viewPage = result as ViewResult;
            PagedList.IPagedList<string> modelList = (viewPage.Model as FizzBuzz).DisplayList;

            // Assert
            modelList.Should().BeEquivalentTo(expectedOutput);
        }

        [TestCase(100, 4, 1, true, false)]
        [TestCase(20, 0, 1, false, false)]
        [TestCase(25, 1, 1, true, false)]
        public void FizzBuzzViewDisplaysPreviousAndNextButtonWhenInputValueIsGiven(int inputNumber, int pageNumber, int linkValue, bool hasPrev, bool hasNext)
        {
            // Act
            ViewResult viewPage = fizbuzzController.GetPages(inputNumber, pageNumber, linkValue) as ViewResult;
            FizzBuzz model = viewPage.Model as FizzBuzz;

            // Assert
            model.DisplayList.HasPreviousPage.Should().Be(hasPrev);
            model.DisplayList.HasNextPage.Should().Be(hasNext);
        }
    }
}