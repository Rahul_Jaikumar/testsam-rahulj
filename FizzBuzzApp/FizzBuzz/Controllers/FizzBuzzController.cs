﻿namespace FizzBuzz.Controllers
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using FizzBuzz.Business.Interfaces;
    using FizzBuzz.Models;
    using PagedList;

    public class FizzBuzzController : Controller
    {
        private const int PAGESIZE = 20;
        private readonly IFizzBuzzBusiness fizzBuzzBusiness;

        public FizzBuzzController(IFizzBuzzBusiness fizzBuzzBusiness)
        {
            this.fizzBuzzBusiness = fizzBuzzBusiness;
        }

        // GET: Fizbuzz
        [HttpGet]
        public ActionResult Fizzbuzz()
        {
            return this.View("Fizzbuzz");
        }

        [HttpPost]
        public ActionResult Fizzbuzz(FizzBuzz model)
        {
            if (this.ModelState.IsValid)
            {
                model.PageNumber = 1;
                model.DisplayList = this.GetFizzBuzzList(model.InputNumber).ToPagedList(1, PAGESIZE);
            }

            return this.View("Fizzbuzz", model);
        }

        public ActionResult GetPages(int inputNumber, int pageNumber, int linkValue)
        {
            FizzBuzz model = new FizzBuzz
            {
                InputNumber = inputNumber,
            };
            model.PageNumber = pageNumber + linkValue;
            model.DisplayList = this.GetFizzBuzzList(model.InputNumber).ToPagedList(model.PageNumber, PAGESIZE);
            return this.View("Fizzbuzz", model);
        }

        private IEnumerable<string> GetFizzBuzzList(int inputNumber)
        {
            return this.fizzBuzzBusiness.GetFizzBuzzList(inputNumber);
        }
    }
}