﻿namespace FizzBuzz.HtmlHelpers
{
    using System.Web;
    using System.Web.Mvc;

    public static class FizzBuzzBuilder
    {
        public static IHtmlString FizzBuzzValueBuilder(this HtmlHelper helper, string fizzBuzzValue)
        {
            if (fizzBuzzValue.Contains(" "))
            {
                string[] fizzSplit = fizzBuzzValue.Split(' ');
                TagBuilder fizzTag = new TagBuilder("span");
                fizzTag.Attributes.Add("class", fizzSplit[0]);
                fizzTag.InnerHtml = fizzSplit[0];
                TagBuilder buzzTag = new TagBuilder("span");
                buzzTag.Attributes.Add("class", fizzSplit[1]);
                buzzTag.InnerHtml = fizzSplit[1];
                return new MvcHtmlString(fizzTag.ToString() + " " + buzzTag.ToString());
            }
            else
            {
                TagBuilder fbTag = new TagBuilder("span");
                fbTag.Attributes.Add("class", fizzBuzzValue);
                fbTag.InnerHtml = fizzBuzzValue;
                return new MvcHtmlString(fbTag.ToString());
            }
        }
    }
}