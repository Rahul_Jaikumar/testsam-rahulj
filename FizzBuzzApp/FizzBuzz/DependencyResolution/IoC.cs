// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IoC.cs" company="Web Advanced">
// Copyright 2012 Web Advanced (www.webadvanced.com)
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------


namespace FizzBuzz.DependencyResolution {
    using FizzBuzz.Business;
    using FizzBuzz.Business.Interfaces;
    using FizzBuzz.Business.Strategies;
    using StructureMap;
    using System;
    using System.Configuration;

    public static class IoC {
        public static IContainer Initialize() {

            DayOfWeek dayOfWeek;
            Enum.TryParse(ConfigurationManager.AppSettings["DayOfWeek"], out dayOfWeek);
            return new Container(x =>
            {
                x.For<IFizzBuzzBusiness>().Use<FizzBuzzBusiness>();
                x.For<IDivisibilityRule>().Use<DivisibileByThreeAndFive>().SetProperty(a => a.Order = 1);
                x.For<IDivisibilityRule>().Use<DivisibileByThree>().SetProperty(a => a.Order = 2);
                x.For<IDivisibilityRule>().Use<DivisibileByFive>().SetProperty(a => a.Order = 3);
                x.For<IDayCheckerRule>().Use<DayChecker>().SetProperty(a => a.SpecifiedDayOfWeek = dayOfWeek);
            });
        }
    }
}