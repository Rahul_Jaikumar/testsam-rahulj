﻿namespace FizzBuzz.Business.Tests
{
    using FizzBuzz.Business.Interfaces;
    using FizzBuzz.Business.Strategies;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;
    using System.Collections.Generic;

    public class FizzBuzzBusinessTests
    {
        private FizzBuzzBusiness businessLogic;
        private IEnumerable<IDivisibilityRule> divisibilityRules;
        private Mock<IDivisibilityRule> divisibilityByThreeMock, divisibilityByFiveMock, divisibilityByThreeAndFiveMock;

        [SetUp]
        public void Setup()
        {
            divisibilityByThreeMock = new Mock<IDivisibilityRule>();
            divisibilityByFiveMock = new Mock<IDivisibilityRule>();
            divisibilityByThreeAndFiveMock = new Mock<IDivisibilityRule>();
            divisibilityByThreeMock.Setup(m => m.IsDivisibleBy(It.Is<int>(x => x % 3 == 0))).Returns(true);
            divisibilityByThreeMock.Setup(m => m.IsDivisibleBy(It.Is<int>(x => x % 3 != 0))).Returns(false);
            divisibilityByFiveMock.Setup(m => m.IsDivisibleBy(It.Is<int>(x => x % 5 == 0))).Returns(true);
            divisibilityByFiveMock.Setup(m => m.IsDivisibleBy(It.Is<int>(x => x % 5 != 0))).Returns(false);
            divisibilityByThreeAndFiveMock.Setup(m => m.IsDivisibleBy(It.Is<int>(x => x % 15 == 0))).Returns(true);
            divisibilityByThreeAndFiveMock.Setup(m => m.IsDivisibleBy(It.Is<int>(x => x % 15 != 0))).Returns(false);
            divisibilityRules = new List<IDivisibilityRule> {
                divisibilityByThreeAndFiveMock.Object,
                divisibilityByThreeMock.Object,
                divisibilityByFiveMock.Object
            };
            businessLogic = new FizzBuzzBusiness(divisibilityRules);
        }

        [Test]
        public void GetFizzBuzzListReturnsValidListForNonWednesday()
        {
            // Arrange
            divisibilityByThreeMock.Setup(m => m.GetFizzBuzzValue()).Returns("Fizz");
            divisibilityByFiveMock.Setup(m => m.GetFizzBuzzValue()).Returns("Buzz");
            divisibilityByThreeAndFiveMock.Setup(m => m.GetFizzBuzzValue()).Returns("Fizz Buzz");
            List<string> expectedOutput = new List<string>() {
                "1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "Fizz Buzz"
            };

            // Act
            IEnumerable<string> actualOutput = businessLogic.GetFizzBuzzList(15);

            // Assert
            actualOutput.Should().BeEquivalentTo(expectedOutput);
        }

        [Test]
        public void GetFizzBuzzListReturnsValidListForWednesday()
        {
            // Arrange
            divisibilityByThreeMock.Setup(m => m.GetFizzBuzzValue()).Returns("Wizz");
            divisibilityByFiveMock.Setup(m => m.GetFizzBuzzValue()).Returns("Wuzz");
            divisibilityByThreeAndFiveMock.Setup(m => m.GetFizzBuzzValue()).Returns("Wizz Wuzz");
            List<string> expectedOutput = new List<string>() {
                "1", "2", "Wizz", "4", "Wuzz", "Wizz", "7", "8", "Wizz", "Wuzz", "11", "Wizz", "13", "14", "Wizz Wuzz"
            };

            // Act
            IEnumerable<string> actualOutput = businessLogic.GetFizzBuzzList(15);

            // Assert
            actualOutput.Should().BeEquivalentTo(expectedOutput);
        }
    }
}