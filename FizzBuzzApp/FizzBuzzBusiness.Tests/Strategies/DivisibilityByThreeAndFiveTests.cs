﻿namespace FizzBuzz.Business.Tests.Strategies
{
    using FizzBuzz.Business.Interfaces;
    using FizzBuzz.Business.Strategies;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;
    using System;

    internal class DivisibilityByThreeAndFiveTests
    {
        private Mock<IDayCheckerRule> dayCheckerMock;
        private DivisibileByThreeAndFive divisibilityByThreeAndFiveTest;

        [SetUp]
        public void Setup()
        {
            dayCheckerMock = new Mock<IDayCheckerRule>();
            divisibilityByThreeAndFiveTest = new DivisibileByThreeAndFive(dayCheckerMock.Object);
        }

        [TestCase(true, "Wizz Wuzz")]
        [TestCase(false, "Fizz Buzz")]
        public void DisplayMessageReturnsFizzBuzzOrWizzWuzz(bool isExpectedResult, string expectedOutput)
        {
            // Arrange
            dayCheckerMock.Setup(m => m.IsSpecifiedDayOfWeek(It.IsAny<DateTime>())).Returns(isExpectedResult);

            // Act
            string actualOutput = divisibilityByThreeAndFiveTest.GetFizzBuzzValue();

            //Assert
            actualOutput.Should().Be(expectedOutput);
        }

        [TestCase(0, true)]
        [TestCase(6, false)]
        [TestCase(15, true)]
        [TestCase(20, false)]
        public void IsDivisibleByReturnsValidOutput(int number, bool expectedOutput)
        {
            // Act
            bool actualOutput = divisibilityByThreeAndFiveTest.IsDivisibleBy(number);

            // Assert
            actualOutput.Should().Be(expectedOutput);
        }
    }
}