﻿namespace FizzBuzz.Business.Tests.Strategies
{
    using FizzBuzz.Business.Interfaces;
    using FizzBuzz.Business.Strategies;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;
    using System;

    internal class DivisibilityByFiveTests
    {
        private Mock<IDayCheckerRule> dayCheckerMock;
        private DivisibileByFive divisibilityByFive;

        [SetUp]
        public void Setup()
        {
            dayCheckerMock = new Mock<IDayCheckerRule>();
            divisibilityByFive = new DivisibileByFive(dayCheckerMock.Object);
        }

        [TestCase(true, "Wuzz")]
        [TestCase(false, "Buzz")]
        public void DisplayMessageReturnsBuzzOrWuzz(bool isExpectedResult, string expectedOutput)
        {
            //Arrange
            dayCheckerMock.Setup(m => m.IsSpecifiedDayOfWeek(It.IsAny<DateTime>())).Returns(isExpectedResult);

            //Act
            string actualOutput = divisibilityByFive.GetFizzBuzzValue();


            //Assert
            actualOutput.Should().Be(expectedOutput);
        }

        [TestCase(0, true)]
        [TestCase(10, true)]
        [TestCase(21, false)]
        public void IsDivisibleByReturnsValidOutput(int number, bool expectedOutput)
        {
            //Act
            bool actualOutput = divisibilityByFive.IsDivisibleBy(number);

            //Assert
            actualOutput.Should().Be(expectedOutput);

        }
    }
}