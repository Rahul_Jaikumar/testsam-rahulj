﻿namespace FizzBuzz.Business.Tests.Strategies
{
    using FizzBuzz.Business.Interfaces;
    using FizzBuzz.Business.Strategies;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;
    using System;

    public class DivisibilityByThreeTests
    {

        private Mock<IDayCheckerRule> dayCheckerMock;
        private DivisibileByThree divisibileByThree;

        [SetUp]
        public void Setup()
        {
            dayCheckerMock = new Mock<IDayCheckerRule>();
            divisibileByThree = new DivisibileByThree(dayCheckerMock.Object);
        }

        [TestCase(true, "Wizz")]
        [TestCase(false, "Fizz")]
        public void DisplayMessageReturnsFizzOrWizz(bool isExpectedResult, string expectedOutput)
        {
            // Arrange
            dayCheckerMock.Setup(m => m.IsSpecifiedDayOfWeek(It.IsAny<DateTime>())).Returns(isExpectedResult);

            // Act
            string actualOutput = divisibileByThree.GetFizzBuzzValue();


            // Assert
            actualOutput.Should().Be(expectedOutput);
        }

        [TestCase(0, true)]
        [TestCase(9, true)]
        [TestCase(10, false)]
        public void IsDivisibleByReturnsValidOutput(int number, bool expectedOutput)
        {
            // Act
            bool actualOutput = divisibileByThree.IsDivisibleBy(number);

            // Assert
            actualOutput.Should().Be(expectedOutput);

        }
    }
}