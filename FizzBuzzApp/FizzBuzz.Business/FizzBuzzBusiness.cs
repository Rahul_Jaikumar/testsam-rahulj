﻿namespace FizzBuzz.Business.Strategies
{
    using FizzBuzz.Business.Interfaces;
    using System.Collections.Generic;
    using System.Linq;

    public class FizzBuzzBusiness : IFizzBuzzBusiness
    {
        private readonly IEnumerable<IDivisibilityRule> divisibilityRule;

        public FizzBuzzBusiness(IEnumerable<IDivisibilityRule> divisibilityList)
        {
            divisibilityRule = divisibilityList.OrderBy(x => x.Order);
        }

        public IList<string> GetFizzBuzzList(int number)
        {
            List<string> list = new List<string>();
            for (int index = 1; index <= number; index++)
            {
                IDivisibilityRule result = divisibilityRule.FirstOrDefault(m => m.IsDivisibleBy(index));
                list.Add((result == null) ? index.ToString() : result.GetFizzBuzzValue());
            }

            return list;
        }
    }
}