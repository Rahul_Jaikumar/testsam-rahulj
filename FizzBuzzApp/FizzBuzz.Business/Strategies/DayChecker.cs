﻿namespace FizzBuzz.Business.Strategies
{
    using FizzBuzz.Business.Interfaces;
    using System;

    public class DayChecker : IDayCheckerRule
    {
        public DayOfWeek SpecifiedDayOfWeek { get; set; }

        public bool IsSpecifiedDayOfWeek(DateTime date)
        {
            return date.DayOfWeek == SpecifiedDayOfWeek;
        }
    }
}