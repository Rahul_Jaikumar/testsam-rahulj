﻿namespace FizzBuzz.Business.Strategies
{
    using FizzBuzz.Business.Interfaces;
    using System;

    public class DivisibileByThreeAndFive : IDivisibilityRule
    {
        private readonly IDayCheckerRule dayCheckerRule;

        public DivisibileByThreeAndFive(IDayCheckerRule dayChecker)
        {
            dayCheckerRule = dayChecker;
        }

        public int Order { get; set; }

        public bool IsDivisibleBy(int number)
        {
            return number % 5 == 0 && number % 3 == 0;
        }

        public string GetFizzBuzzValue()
        {
            return dayCheckerRule.IsSpecifiedDayOfWeek(DateTime.Now) ? "Wizz Wuzz" : "Fizz Buzz";
        }
    }
}