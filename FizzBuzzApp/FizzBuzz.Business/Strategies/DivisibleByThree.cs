﻿namespace FizzBuzz.Business.Strategies
{
    using FizzBuzz.Business.Interfaces;
    using System;

    public class DivisibileByThree : IDivisibilityRule
    {
        private readonly IDayCheckerRule dayCheckerRule;

        public DivisibileByThree(IDayCheckerRule dayChecker)
        {
            dayCheckerRule = dayChecker;
        }

        public int Order { get; set; }

        public bool IsDivisibleBy(int number)
        {
            return number % 3 == 0;
        }

        public string GetFizzBuzzValue()
        {
            return dayCheckerRule.IsSpecifiedDayOfWeek(DateTime.Now) ? "Wizz" : "Fizz";
        }
    }
}


